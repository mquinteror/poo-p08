package unam.fi.poo.aplicacion;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.JLabel;
import javax.swing.JButton;
import unam.fi.poo.eventos.ManejadorEventos;
public class CAD extends JFrame {
   
    public CAD(){
        super();
        super.setTitle("CAD Swing - Diseño Asistido por Computadora ");
        super.setSize(600,300);
        this.setLayout (new BorderLayout());
        
        this.add(new JLabel("Bienvenido al Graficador"), BorderLayout.NORTH);

        JButton btn = new JButton("Saluda!");
        btn.addActionListener(new ManejadorEventos());
        this.add(btn);

        super.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        super.setVisible(true);
    }

    public static void main(String[] args){
        new CAD();
    }
}
