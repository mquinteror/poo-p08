package unam.fi.poo.aplicacion;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import unam.fi.poo.eventos.ManejadorEventos;
public class CAD extends Application{
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("Saluda!");
        btn.setOnAction(new ManejadorEventos());
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);

        Scene scene = new Scene(root, 600, 250);

        primaryStage.setTitle("CAD - Diseño asistido por computadora JavaFX");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args){
        launch(args);
    }
}
